import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { Activity } from '../entity/activity';
import { EnrolledTableDataSource } from './enrolled-table-datasource';
import { TeacherService } from '../service/teacher-service';
import { AppService } from '../service/app-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enrolled',
  templateUrl: './enrolled.component.html',
  styleUrls: ['./enrolled.component.css']
})
export class EnrolledComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: EnrolledTableDataSource;
  displayedColumns = ['name', 'description', 'location', 'host', 'period', 'date',];
  activities: any[];
  as: Array<any> = [];
  filter: string;
  filter$: BehaviorSubject<string>;
  disabled: boolean[] = [];
  currentUser: any;
  isEdit: boolean;

  editId: string;
  name: string;
  location: string;
  description: string;
  date: any;
  period: any;
  host: string;
  students: string[]

  constructor(private appService: AppService) {

  }
  ngOnInit() {

    this.currentUser = JSON.parse(localStorage.getItem('student'));

    this.appService.getActivities().subscribe(data => {
      this.activities = data.map(e => {

        return {
          date: e.payload.doc.data()['date'],
          description: e.payload.doc.data()['description'],
          host: e.payload.doc.data()['host'],
          location: e.payload.doc.data()['location'],
          name: e.payload.doc.data()['name'],
          period: e.payload.doc.data()['period'],
          students: e.payload.doc.data()['students'],
          id: e.payload.doc.id
        };



      });

      this.dataSource = new EnrolledTableDataSource();
      this.dataSource.data = this.as;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;


      for (let i = 0; i < this.activities.length; i++) {
        if(this.activities[i].students.indexOf(this.currentUser.studentId)!= -1){
          this.as.push(this.activities[i]);
        }
      }

    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
