import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core'


@Injectable({
    providedIn: 'root'
})

export class TeacherService {
    constructor(private firestore: AngularFirestore) { }

    getConfirmation() {
        return this.firestore.collection('confirmation').snapshotChanges();
    }

    confirm(activity: string) {
        return this.firestore.doc('confirmation/' + activity).delete();
    }

    update(record, id) {
        return this.firestore.doc('activity/' + id).update(record);
    }

    
}
