import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  currentUser: any;
  constructor(private router: Router) {

  }

  ngOnInit() {
    if (localStorage.getItem('admin') == null) {
      this.router.navigate(['/login']);
      alert('Please login');
    }
    this.currentUser = localStorage.getItem('admin');
  }
}
