import { Teacher } from './teacher';
import { Student } from './student';

export class Activity {
    name: string;
    location: string;
    description: string;
    period: Date;
    date: Date;
    host: string;
    students: Student [];
    id: string;
}
