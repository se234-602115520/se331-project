import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AppService } from '../service/app-service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  currentUser: {};
  studentAuth: any[];
  teacherAuth: any[];
  selected = '';
  roles = [
    { value: 'admin', viewValue: 'Administrator' },
    { value: 'teacher', viewValue: 'Teacher' },
    { value: 'student', viewValue: 'Student' }
  ];
  constructor(private router: Router, private afAuth: AngularFireAuth, private appService: AppService) {
  }

  ngOnInit() {
    this.appService.getStudents().subscribe(data => {
      this.studentAuth = data.map(e => {
      
        return {
          name: e.payload.doc.data()['name'],
          surname: e.payload.doc.data()['surname'],
          image: e.payload.doc.data()['image'],
          birthday: e.payload.doc.data()['birthday'],
          email: e.payload.doc.data()['email'],
          password: e.payload.doc.data()['password'],
          studentId: e.payload.doc.data()['studentId'],
          activities: e.payload.doc.data()['activities'],
          id: e.payload.doc.id
        };
      })
    });
   
    this.appService.getTeachers().subscribe(data => {
      this.teacherAuth = data.map(e => {
        return {
          name: e.payload.doc.data()['name'],
          surname: e.payload.doc.data()['surname'],
          email: e.payload.doc.data()['email'],
          password: e.payload.doc.data()['password'],    
          activities: e.payload.doc.data()['activities'],
          id: e.payload.doc.id,
        };
      })
    });
  }

  login() {
    if (this.selected === 'admin') {
      this.adminLogin();
    } else if (this.selected === 'teacher') {
      this.teacherLogin();
    } else if (this.selected === 'student') {
      this.studentLogin();
    }

  }

  register() {
    return this.router.navigate(['/register']);
  }

  async adminLogin() {
    const { username, password, } = this;
    try {
      const res = await this.afAuth.auth.signInWithEmailAndPassword(username, password)
      
      localStorage.setItem('admin', username);
      return this.router.navigate(['/admin']);
    } catch (err) {
      alert('Login failed!');
    
    }
  }

  async teacherLogin() {
    for (let i = 0; i < this.teacherAuth.length; i++) {
      if(this.teacherAuth[i].email == this.username && this.teacherAuth[i].password == this.password){
          this.currentUser = {
            email: this.teacherAuth[i].email,
            activities: this.teacherAuth[i].activities,
            id: this.teacherAuth[i].id,
            name: this.teacherAuth[i].name,
            surname: this.teacherAuth[i].surname
          }
          localStorage.setItem('teacher', JSON.stringify(this.currentUser));
          return this.router.navigate(['teacher/' + this.username]);
      }
  }
  }

  async studentLogin() {

    for (let i = 0; i < this.studentAuth.length; i++) {
        if(this.studentAuth[i].email == this.username && this.studentAuth[i].password == this.password){
            this.currentUser = {
              email: this.studentAuth[i].email,
              activities: this.studentAuth[i].activities,
              surname: this.studentAuth[i].surname,
              name: this.studentAuth[i].name,
              id: this.studentAuth[i].id,
              birthday: this.studentAuth[i].birthday,
              password: this.studentAuth[i].password,
              image: this.studentAuth[i].image,
              studentId: this.studentAuth[i].studentId
            }
            localStorage.setItem('student', JSON.stringify(this.currentUser));
            return this.router.navigate(['student/' + this.username]);
        }
    }
  }
}

