import { Activity } from './activity';

export class Student {
    name: string;
    surname: string;
    image: string;
    birthday: Date;
    studentId: string;
    email: string;
    password: string;
    activities: Activity [];
    id: string
}
